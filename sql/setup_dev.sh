export LIQUIBASE_COMMAND_URL=jdbc:oracle:thin:@demo-db1-rhel6.datical.net:1521/PP_DEV.datical.net
export LIQUIBASE_COMMAND_USERNAME=DATICAL_USER
export LIQUIBASE_COMMAND_PASSWORD=DATICAL_USER_PW
export LIQUIBASE_HUB_MODE=off
export LIQUIBASE_HUB_API_KEY=02HcmDJzX_tg0YfTL9ecAE2M0PNbdnjbiNV1dWgbphU
export LIQUIBASE_COMMAND_CHANGELOG_FILE=sql/changelog_master.xml
export LIQUIBASE_PRO_LICENSE_KEY=<paste your LIQUIBASE_PRO_LICENSE_KEY here>

liquibase checks show
liquibase checks bulk-set --disable
liquibase checks delete --check-name=SqlGrantAlterWarn
liquibase checks enable --check-name=SqlGrantSpecificPrivsWarn
SqlGrantAlterWarn
2
"ALTER SESSION","ALTER SYSTEM",EXP_FULL_DATABASE,IMP_FULL_DATABASE,"CREATE ANY TABLE","DROP ANY TABLE","ALTER ANY TABLE","SELECT ANY TABLE","COMMENT ANY TABLE","EXECUTE ANY PROCEDURE"


--changeset adeel:04
GRANT ALTER SESSION TO LB_APP_USER;


--changeset adeel:05
GRANT EXP_FULL_DATABASE TO LB_APP_USER;
GRANT IMP_FULL_DATABASE TO LB_APP_USER;

--changeset adeel:07
GRANT SELECT ANY TABLE TO TEST;

--changeset adeel:08
GRANT COMMENT ANY TABLE TO LB_APP_USER;

--changeset adeel:10
GRANT EXECUTE ANY TABLE TO LB_APP_USER;

--changeset adeel:11
GRANT ALTER ANY TABLE TO LB_APP_USER;

--changeset adeel:11
GRANT EXECUTE ANY PROCEDURE TO LB_APP_USER;

--changeset adeel:16
SELECT * FROM DATABASECHANGELOG;

--changeset adeel:19
COMMENT ON TABLE Employee IS 'This is a table for Employee.';


--changeset adeel:26
create table tablespace (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
);

create table zone (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
);

create table UNLIMITED (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
);